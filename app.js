var $header = $('.header'),
    $settings = $('.settings'),
    $modal = $('.modal'),
    $modalOverlay = $('.modal__overlay'),
    $openModalBtn = $('.subheader__icon');

$header.click(function() {
    $(this).toggleClass('header__hiden-settings');
    $settings.slideToggle();
});

$openModalBtn.click(function() {
    $modal.fadeIn();
});
$modalOverlay.click(function() {
    $modal.fadeOut();
});