# Task 1

Create static section template. All text should be hardcoded
Click on header should expand/close sub menu
List items should be links.
Items should be links (whole block, not only item name)
Click on projector screen, should raise alert with list of colors above (optional - Create centered modal dialog instead of alert, it should be closable on overlay)

[Click here to view online](https://naivok.gitlab.io/task-1-section/section.html)